DATADIR=$(shell realpath data)

ATRAFS_MONTHS=2001_9 2007_10 2013_3 2013_4 2013_5 2013_6 2013_7 2013_8 2013_9 2014_1 2014_2 2014_3
ATRAFS=$(ATRAFS_MONTHS:%=data/On_Time_On_Time_Performance_%.csv)

FETCH=wget -q -O
ATRAF_DATA_URL=https://s3.eu-central-1.amazonaws.com/atraf/atraf-data

default:
	true

data-ints1x5M: data/ints5M.csv data/ints1x5M.sql
data-ints10x500K: data/ints500K.csv data/ints10x500K.sql
data-ints10x500Ksep: data/ints500K.csv data/ints10x500Ksep.sql
data-atraf12: $(ATRAFS:@=data/) data/atraf12.sql
data-atraf12sep: $(ATRAFS:@=data/) data/atraf12sep.sql
data-atraf12combi: data/On_Time_combined.csv data/atraf12combi.sql
data-sf-%: data/sf-%.sql
	@echo ok

data/ints5M.csv: data/ints500K.csv
	rm -f $@.tmp
	for i in 1 2 3 4 5 6 7 8 9 10; do cat $< >>$@.tmp; done
	mv $@.tmp $@

data/ints500K.csv: gen_ints.py data/.dir
	./$< 500''000 >$@.tmp
	mv $@.tmp $@

data/On_Time_combined.csv: $(ATRAFS)
	rm -f $@.tmp
	sed -n -e 1p $< >$@.tmp
	sed -e '/^"/d' $^ >>$@.tmp
	mv $@.tmp $@

data/On_Time%.csv: data/On_Time%.csv.gz
	gzip -d <$< >$@.tmp
	mv $@.tmp $@
data/On_Time%.csv.gz: data/.dir
	$(FETCH) $@.tmp $(ATRAF_DATA_URL)/$(@:data/%=%)
	mv $@.tmp $@

data/sf-%.sql: data/sf-%/counts
	echo 'START TRANSACTION;' >$@.tmp
	sed $< -e 's,^ *,,' -e 's,[.]tbl$$,,' -e 's,\([0-9]*\) ./\(.*\),COPY \1 RECORDS INTO \2 FROM '"'$(PWD)/"'$(@:.sql=)/\2.tbl'"';," >>$@.tmp
	echo 'COMMIT;' >>$@.tmp
	mv $@.tmp $@

data/sf-%/counts: data/sf-%/.dir
	test -d $(DBGENDIR) # Do not forget to set DBGENDIR
	cd $(DBGENDIR) && ./dbgen -vf -s $(@:data/sf-%/counts=%)
	cd $$(dirname '$@') && cp -v $(DBGENDIR)/*.tbl . && wc -l ./*.tbl | grep / >counts.tmp
	mv $@.tmp $@

data/sf-%/.dir:
	mkdir -p $(@:.dir=)
	touch $@

data/%.sql: experiments/%.sql.in data/.dir
	sed -e "s|@|$(DATADIR)|" <$< >$@.tmp
	mv $@.tmp $@

data/.dir:
	mkdir -p data
	touch $@


.PRECIOUS: data/.dir data/sf-%/.dir data/sf-%/counts data/sf-%.sql
