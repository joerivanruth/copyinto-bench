#!/usr/bin/env python3

import sys

COLS = 40
ROWS = int(sys.argv[1])

for r in range(ROWS):
	for c in range(1, COLS + 1):
		print(100 * r + c, end='|')
	print()