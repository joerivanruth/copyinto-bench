#!/usr/bin/env python3

# this is just an example

import os
from shlex import quote

iterations = 20
csv_file = 'results.csv'

experiments = [
    'ints1x5M',
    'ints10x500K',
    'ints10x500Ksep',
	'atraf12combi',
    'atraf12',
    'atraf12sep',
	'sf-1',
	'sf-5',
]

thread_counts = [
	1, 2, 3, 4, 6, 7, 8,
	9, 10, 11, 12,
	14, 16, 18, 20,
	24, 28, 32, 36,
	40, 44, 48
]

print('#!/bin/bash')
print()

for thread_count in thread_counts:
	n = iterations
	if thread_count < 8:
		n = n * thread_count // 8
	if n < 5:
		n = 5
	numas = []
	if thread_count <= 24:
		numas.append('tight')
	if thread_count <= 8:
		numas.append('bound')
	numas.append('free')
	for numa in numas:
		if numa == 'tight':
			cpus = thread_count
		elif numa == 'bound':
			cpus = 8
		else:
			cpus = None
		if cpus:
			numactl = ['numactl', '--physcpubind', f'0-{cpus-1}', '--']
		else:
			numactl = []
		for version in ['default', 'copyparpipe']:
			tag = f'{numa},{thread_count}'
			root = os.path.join(os.environ['MONETDB_ENVS'], version + '_rel', 'inst')
			cmd = [
				'./once.sh', 'completed.list', 'failed.list',
				*numactl,
				'./bench.py',
				'-f', csv_file,
				'-t', tag,
				'-n', n,
				'-r', root,
				'--opt', f'gdk_nr_threads={thread_count}',
				*experiments,
			]
			quoted = ' '.join(quote(str(word)) for word in cmd)
			print(quoted)
