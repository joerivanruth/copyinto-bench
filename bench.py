#!/usr/bin/env python3

import argparse
import datetime
import os
import shutil
import socket
import subprocess
from collections import defaultdict
import time

argparser = argparse.ArgumentParser()
argparser.add_argument(
    '-l', '--list', help='list available experiments', action='store_true')

server_args = argparser.add_mutually_exclusive_group()
server_args.add_argument(
    "-d", "--database", help="MAPI URL of running mserver")
server_args.add_argument(
    "-r", "--root", help="Installation root of MonetDB to start")

argparser.add_argument("-p", "--port", help="Port number to start mserver5 on",
                       type=int)
argparser.add_argument('--dbpath', help='DB path to start mserver5 on',
                       default='dbdir')

argparser.add_argument("--opt", help="Option to set on mserver5",
                       action="append")

argparser.add_argument('-n', '--runs', default=10, type=int,
                       help='Number of times to run each test')
argparser.add_argument('-f', '--file',
                       help='CSV file to write results to')
argparser.add_argument('-t', '--tag',
                       help='Tag to use in CSV rows')
argparser.add_argument('experiments', metavar='EXPERIMENTS', nargs='*')

HOST = socket.gethostname()


def pick_port():
    # pick a free port number
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(('', 0))
    port = s.getsockname()[1]
    s.close()
    return port


def start_mserver5(root_dir, dbpath, port, opts):
    env = os.environ.copy()
    if root_dir:
        path_entry = os.path.join(root_dir, 'bin')
        mserver5 = os.path.join(path_entry, 'mserver5')
        if not os.path.exists(mserver5):
            exit(f"Root {root_dir} does not have {mserver5}")
        env['PATH'] = path_entry + os.pathsep + env['PATH']
        lib_path = env.get('LD_LIBRARY_PATH')
        lib_path_entry = os.path.join(root_dir, 'lib')
        if lib_path:
            lib_path = os.pathsep + lib_path
        else:
            lib_path = ''
        lib_path = lib_path_entry + lib_path
        env['LD_LIBRARY_PATH'] = lib_path

    if os.path.isdir(dbpath):
        if os.path.isdir(os.path.join(dbpath, 'sql_logs')):
            shutil.rmtree(dbpath)
        else:
            os.rmdir(dbpath)  # will fail if there is any content

    cmd = ['mserver5', f'--dbpath={dbpath}']
    if not port:
        port = pick_port()
    for o in [f'mapi_port={port}', *opts]:
        cmd += ['--set', o]
    proc = subprocess.Popen(cmd, env=env, stdout=subprocess.DEVNULL)
    for i in range(100):
        code = proc.poll()
        if code != None:
            exit(f'{cmd} exited with code {code}')
        if os.path.exists(os.path.join(dbpath, '.started')):
            break
        time.sleep(0.1)
    else:
        proc.kill()
        print()
        exit(f'Terminated {cmd} because it failed to come up')

    url = f'mapi:monetdb://localhost:{port}/'
    return (url, proc, env)


class Report:
    def __init__(self, filename, tag: str):
        self.filename = filename
        self.file = open(filename, 'a')
        self.tag = tag
        self.measurements = defaultdict(lambda: [])

    def report(self, exp: str, n: int, duration: float):
        self.measurements[exp].append(duration)
        now = datetime.datetime.now().strftime("%a %Y-%m-%d %H:%M:%S")
        print(f'{HOST},{self.tag},"{now}",{exp},{n},{duration}',
              file=self.file, flush=True)

    def dump(self):
        total_count = sum(len(values) for values in self.measurements.values())
        nexps = len(self.measurements.keys())
        print()
        print(f"RAN {nexps} EXPERIMENTS, {total_count} RUNS")
        for exp, data in sorted(self.measurements.items()):
            average = sum(data) / len(data)
            print(f"   {exp}: n={len(data)} avg={average:.2f}")


def run_one(exp, n: int, report: Report, dry_run=False):
    print(f"EXP {exp} ({n}) ", end='', flush=True)
    to_kill = None
    env = None
    try:
        if args.database:
            dburl = args.database
        else:
            dburl, to_kill, env = start_mserver5(
                args.root, args.dbpath, args.port, args.opt or [])
        out = None if dry_run else subprocess.DEVNULL
        subprocess.check_call(['./prep.sh', dburl, exp],
                              stdout=out, stderr=out, env=env)
        if dry_run:
            return
        t0 = time.time()
        subprocess.check_call(
            ['./run.sh', dburl, exp], stdout=out, stderr=out, env=env)
        t1 = time.time()
        duration = t1 - t0
        report.report(exp, n, t1 - t0)
        print(f"DURATION {duration:.2f}")
    except subprocess.CalledProcessError as e:
        # This doesn't work, don't know why
        print()
        if e.stdout:
            print('--- start stdout ---')
            print(e.stdout)
            print('--- end stdout ---')
        if e.stderr:
            print('--- start stderr ---')
            print(e.stderr)
            print('--- end stderr ---')
        raise e
    finally:
        if to_kill:
            to_kill.kill()


def work(args):

    for exp in args.experiments:
        print(f"INITIAL ", end="")
        run_one(exp, 0, None, dry_run=True)
        print()

    report = Report(args.file, args.tag)
    try:
        for i in range(1, args.runs + 1):
            for exp in args.experiments:
                run_one(exp, i, report)
    finally:
        report.dump()


def list_experiments(args):
    print("Available experiments:")
    print("    sf-NN           TPC-H SF-NN for any NN")
    for e in os.listdir('experiments'):
        suffix = ".sql.in"
        if e.endswith(suffix):
            e = e[:-len(suffix)]
        else:
            continue
        print("    " + e)


def main(args):
    if args.list:
        return list_experiments(args)
    else:
        if args.tag is None:
            argparser.print_usage()
            exit('error: --tag is mandatory')
        if args.file is None:
            argparser.print_usage()
            exit('error: --file is mandatory')
        if not args.experiments:
            argparser.print_usage()
            exit('error: list at least one experiment')
    try:
        work(args)
    except subprocess.CalledProcessError as e:
        print(e)
        raise SystemExit(1)


if __name__ == "__main__":
    args = argparser.parse_args()
    # print(args)
    main(args)
