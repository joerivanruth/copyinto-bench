#!/bin/bash

USAGE="prep.sh MAPI_URL EXPERIMENT"
url="${1?$USAGE}"
exp="${2?$USAGE}"

script="data/$exp.sql"

set -e
set -x

make -j "data-$exp"

test -f "$script"

mclient -d "$url" prep.sql
