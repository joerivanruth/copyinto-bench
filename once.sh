#!/bin/bash

USAGE="runexp.sh COMPLETIONFILE FAILUREFILE CMD ARGS..."

COMPLETIONFILE="${1?$USAGE}"
FAILUREFILE="${2?$USAGE}"
shift
shift
CMD="$*"

set -e -x

test -f "$COMPLETIONFILE" || touch "$COMPLETIONFILE"

if fgrep -x -q -e "$CMD" "$COMPLETIONFILE"; then
	echo "SKIP $CMD"
	exit 0
fi

echo "RUN $CMD"

if "$@"; then
	echo "$CMD" >> "$COMPLETIONFILE"
	exit 0
else
	echo "$CMD" >> "$FAILUREFILE"
	exit $?
fi