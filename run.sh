#!/bin/bash

USAGE="prep.sh MAPI_URL EXPERIMENT"
url="${1?$USAGE}"
exp="${2?$USAGE}"

script="data/$exp.sql"

set -e
set -x

test -f "$script"

mclient -d "$url" "$script"